package broker;

import common.AClient;
import common.Color;
import common.Settings;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.PrintStream;
import java.net.Socket;
import java.util.Scanner;

public class Broker extends AClient{
    private String marketId = ""; // id of the current market
    private boolean validCmd;

    public static void main(String[] args) throws Throwable {
        Broker broker = new Broker();
        broker.clientType = "broker";

        if (args.length == 1) {
            broker.setId(args[0]);
            broker.start();
        }
        else {
            System.out.println("Usage: java -jar broker.jar BROKER_ID");
        }
    }

    // [TODO] validate command before sending to router
    private void start() throws Throwable {
        Socket s = new Socket(Settings.host, Settings.portBroker);

        String input = "";
        Scanner readInput = new Scanner(System.in);
        PrintStream ps = new PrintStream(s.getOutputStream());
        BufferedReader br = new BufferedReader(new InputStreamReader(s.getInputStream()));
        String line;

        ps.println(this.id);
        this.showMessage("broker " + this.id + " registered to router");

        while (!input.equals("quit")) {
            String marketToken = this.marketId.equals("") ? "" : Color.ANSI_CONSOLE_1 + "@" + Color.ANSI_RESET + this.marketId;
            System.out.print(Color.ANSI_CONSOLE_1 + "[" + Color.ANSI_RESET + this.id + marketToken + Color.ANSI_CONSOLE_1 + "]" + Color.ANSI_RESET + " :> ");
            input = readInput.nextLine();
            String[] cmd;
            boolean goodCmd = false;
            this.validCmd = true;

            switch (input) {
                case "quit":
                    ps.println(this.id + " CMD_QUIT");
                    System.out.println("Bye broker " + this.id);
                    goodCmd = true;
                    break;
                case "markets":
                    System.out.println("List of online markets");
                    goodCmd = true;
                    break;
                case "disconnect":
                    if (this.marketId.length() > 0) {
                        System.out.println("Disconnecting from current market");
                        this.marketId = "";
                    }
                    else {
                        this.messNoMarketSelected();
                    }
                    goodCmd = true;
                    break;
                case "goods":
                    if (this.marketId.length() > 0) {
                        System.out.println("List of goods that can be traded on current market");
                    }
                    else {
                        this.messNoMarketSelected();
                    }
                    goodCmd = true;
                    break;
                case "buy":
                    if (this.marketId.length() > 0) {
                        System.out.println("List of your BUY ORDERS on current market");
                    }
                    else {
                        this.messNoMarketSelected();
                    }
                    goodCmd = true;
                    break;
                case "sell":
                    if (this.marketId.length() > 0) {
                        System.out.println("List of your SELL ORDERS on current market");
                    }
                    else {
                        this.messNoMarketSelected();
                    }
                    goodCmd = true;
                    break;
                default:
                    cmd = input.split("\\s+");
                    if (cmd.length > 1) {
                        switch (cmd[0]) {
                            case "connect":
                                if (cmd.length == 2) {
                                        System.out.println("Connecting to market " + cmd[1]);
                                        this.marketId = cmd[1]; // [TODO] validate connection to market on server side !
                                    goodCmd = true;
                                }
                                break;
                            case "buy":
                                if (cmd[1].equals("orders")) {
                                    if (this.marketId.length() > 0) {
                                        System.out.println("List of ALL BUY orders on current market");
                                    }
                                    else {
                                        this.messNoMarketSelected();
                                    }
                                    goodCmd = true;
                                }
                                else if (cmd.length == 4) {
                                    if (this.marketId.length() > 0) {
                                        System.out.println("Place NEW BUY order on current market: good_id: " + cmd[1] + ", price: " + cmd[2] + ", quantity: " + cmd[3]);
                                    }
                                    else {
                                        this.messNoMarketSelected();
                                    }
                                    goodCmd = true;
                                }
                                break;
                            case "sell":
                                if (cmd[1].equals("orders")) {
                                    if (this.marketId.length() > 0) {
                                        System.out.println("List of ALL SELL orders on current market");
                                    }
                                    else {
                                        this.messNoMarketSelected();
                                    }
                                    goodCmd = true;
                                }
                                else if (cmd.length == 4) {
                                    if (this.marketId.length() > 0) {
                                        System.out.println("Place NEW SELL order on current market: good_id: " + cmd[1] + ", price: " + cmd[2] + ", quantity: " + cmd[3]);
                                    }
                                    else {
                                        this.messNoMarketSelected();
                                    }
                                    goodCmd = true;
                                }
                                break;
                            case "update":
                                if (cmd.length == 4) {
                                    if (cmd[1].equals("buy")) {
                                        if (this.marketId.length() > 0) {
                                            System.out.println("UPDATE BUY order with id " + cmd[2] + ", new price: " + cmd[3] + " on current market");
                                        }
                                        else {
                                            this.messNoMarketSelected();
                                        }
                                        goodCmd = true;
                                    }
                                    else if (cmd[1].equals("sell")) {
                                        if (this.marketId.length() > 0) {
                                            System.out.println("UPDATE SELL order with id " + cmd[2] + ", new price: " + cmd[3] + " on current market");
                                        }
                                        else {
                                            this.messNoMarketSelected();
                                        }
                                        goodCmd = true;
                                    }
                                }
                                break;
                            case "cancel":
                                if (cmd.length == 3) {
                                    if (cmd[1].equals("buy")) {
                                        if (this.marketId.length() > 0) {
                                            System.out.println("CANCEL BUY order with id " + cmd[2] + " on current market");
                                        }
                                        else {
                                            this.messNoMarketSelected();
                                        }
                                        goodCmd = true;
                                    }
                                    else if (cmd[1].equals("sell")) {
                                        if (this.marketId.length() > 0) {
                                            System.out.println("CANCEL SELL order with id " + cmd[2] + " on current market");
                                        }
                                        else {
                                            this.messNoMarketSelected();
                                        }
                                        goodCmd = true;
                                    }
                                }
                                break;
                        }
                    }
            }

            if (goodCmd) {
                if (this.validCmd) {
                    // SEND
                    ps.println(this.id + " " + input);

                    // RECEIVE
                    if ((line = br.readLine()) != null) {
                        this.showMessage("response: " + line);
                    }
                }
            }
            else {
                this.showHelp();
            }
        }
    }

    private void messNoMarketSelected() {
        System.out.print("Please connect to a market first !\n");
        this.validCmd = false;
    }

    private void showHelp() {
        System.out.print("Commands:\n" +
                "                  markets  list online markets\n" +
                "        connect MARKET_ID  connect to selected market (disconnect from current market if connected)\n" +
                "               disconnect  disconnect from current market\n" +
                "               buy orders  list ALL buy orders on connected market\n" +
                "              sell orders  list ALL sell orders on connected market\n" +
                "                      buy  list YOUR buy orders on connected market\n" +
                "                     sell  list YOUR sell orders on connected market\n" +
                "                    goods  list of goods that can be transacted on connected market\n" +
                "   buy GOOD_ID PRICE QTTY  place NEW BUY order on connected market\n" +
                "  sell GOOD_ID PRICE QTTY  place NEW SELL order on connected market\n" +
                "  update buy BUY_ID PRICE  UPDATE price for BUY order on connected market\n" +
                "update sell SELL_ID PRICE  UPDATE price for SELL order on connected market\n" +
                "        cancel buy BUY_ID  CANCEL BUY order on connected market\n" +
                "      cancel sell SELL_ID  CANCEL SELL order on connected market\n" +
                "                     quit\n");
    }
}
