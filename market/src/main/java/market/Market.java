package market;

import common.AClient;
import common.Settings;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.PrintStream;
import java.net.Socket;

public class Market extends AClient {
    public static void main(String[] args) throws Throwable {
        Market market = new Market();
        market.clientType = "market";

        if (args.length == 1) {
            market.setId(args[0]);
            market.start();
        }
        else {
            System.out.println("Usage: java -jar market.jar MARKET_ID");
        }
    }

    private void start() throws Throwable {
        Socket s = new Socket(Settings.host, Settings.portMarket);
        PrintStream ps = new PrintStream(s.getOutputStream());
        BufferedReader br = new BufferedReader(new InputStreamReader(s.getInputStream()));
        String line = null;

        ps.println(this.id);
        this.showMessage("market " + this.id + " registered to router");

        while ((line = br.readLine()) != null) {
            this.showMessage("request: " + line);
            /*
            switch (line) {
                case "buy":
                    ps.println("buying stuff");
                    break;
                case "sell":
                    ps.println("selling stuff");
                    break;
                default:
                    ps.println("default MARKET response");
                    System.out.println("Data sent to server");
            }
            */
            ps.println("default MARKET response");
        }
    }
}
