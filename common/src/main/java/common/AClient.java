package common;

abstract public class AClient {
    protected String clientType;
    protected String id;

    protected void showMessage(String message) {
        System.out.println(Color.ANSI_CONSOLE_1 + "[" + Color.ANSI_RESET + clientType + Color.ANSI_CONSOLE_1 + "] " + Color.ANSI_RESET + message);
    }

    // GETTER && SETTER
    public void setId(String id) {
        this.id = id;
    }

    public String getId() {
        return this.id;
    }
}
