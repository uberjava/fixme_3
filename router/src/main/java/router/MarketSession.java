package router;

import common.Color;

import java.nio.ByteBuffer;

/*
 * Handle clients of type MARKET
 */
class MarketSession extends ClientSession {
    void read() {
        if (this.server.getOperationRead(this.selkey)) {
            try {
                this.buf.clear();
                int readCount = this.chan.read(this.buf);

                if (readCount < 1)
                    return;

                if (readCount > 0) {
                    this.buf.flip();
                    byte[] subStringBytes = new byte[readCount];
                    this.buf.get(subStringBytes);
                    String s = new String(subStringBytes);
                    s = s.substring(0, s.length() - 1); // TO REMOVE \n

                    // IF NOT REGISTERED, GET ID
                    if (!this.isRegistered) {
                        this.id = s;
                        this.server.setOperationRead(this.selkey);
                        this.server.addMessage(Color.ANSI_GREEN + "[m session] " + Color.ANSI_RESET + "new market ID: " + s);
                        //System.out.print(Color.ANSI_GREEN + "\n[m session] " + Color.ANSI_RESET + "new market ID: " + s);
                    }
                    else {
                        this.server.setOperationWrite(this.selkey);
                        this.server.addMessage(Color.ANSI_GREEN + "[m session] " + Color.ANSI_RESET + "sent by market: " + s);
                        //System.out.print(Color.ANSI_GREEN + "\n[m session] " + Color.ANSI_RESET + "sent by market: " + s);
                    }
                }
            } catch (Throwable t) {
                disconnect();
                t.printStackTrace();
            }
        }
    }

    void write() {
        if (this.server.getOperationWrite(this.selkey)) {
            try {
                String marketRequest = "request to market from broker\n";

                ByteBuffer response = ByteBuffer.wrap(marketRequest.getBytes());
                this.chan.write(response);
                this.server.addMessage(Color.ANSI_GREEN + "[m session] " + Color.ANSI_RESET + "done sending request to market");
                this.server.setOperationRead(this.selkey);
            } catch (Throwable t) {
                disconnect();
                t.printStackTrace();
            }
        }
    }
}
