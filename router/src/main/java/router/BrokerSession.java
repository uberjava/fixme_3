package router;

import common.Color;

import java.nio.ByteBuffer;

/*
 * Handle clients of type BROKER
 */
class BrokerSession extends ClientSession {
    private String marketId; // id of the current market

    void read() {
        if (this.server.getOperationRead(this.selkey))
        {
            try {
                this.buf.clear();
                int readCount = this.chan.read(this.buf);

                if (readCount < 1)
                    return;

                if (readCount > 0) {
                    this.buf.flip();
                    byte[] subStringBytes = new byte[readCount];
                    this.buf.get(subStringBytes);

                    String s = new String(subStringBytes);
                    s = s.substring(0, s.length() - 1); // TO REMOVE \n

                    // IF NOT REGISTERED, GET ID
                    if (!this.isRegistered) {
                        this.id = s;
                        this.server.setOperationRead(this.selkey);
                        this.server.addMessage(Color.ANSI_GREEN + "[b session] " + Color.ANSI_RESET + "new broker ID: " + s);
                        //System.out.print(Color.ANSI_GREEN + "\n[m broker] " + Color.ANSI_RESET + "new broker ID: " + s);
                    }
                    else {
                        String[] input = s.split("\\s+"); // input[1] is client id
                        switch (input[1]) {
                            case "CMD_QUIT":
                                this.isRegistered = false;
                                this.isRemoved = true;
                                this.server.addMessage(Color.ANSI_GREEN + "[b session] " + Color.ANSI_RESET + "disconnecting broker " + input[0]);
                                this.disconnect();
                                break;
                            case "markets":
                                this.server.addMesageToSendOnWriteOperation(this.selkey, this.server.getClientsFromTheOtherserver());
                                this.server.setOperationWrite(this.selkey);
                                break;
                            case "connect":
                                // [TODO] validate market ID !
                                this.marketId = input[2];
                                this.server.addMesageToSendOnWriteOperation(this.selkey, "market selected: " + this.marketId + "\n");
                                this.server.setOperationWrite(this.selkey);
                                break;
                            case "disconnect":
                                this.marketId = "";
                                this.server.addMesageToSendOnWriteOperation(this.selkey, "disconnected from market\n");
                                this.server.setOperationWrite(this.selkey);
                                break;
                            default:
                                this.server.setOperationWrite(this.selkey);
                                this.server.addMessage(Color.ANSI_GREEN + "[b session] " + Color.ANSI_RESET + "sent by broker: " + s);
                        }

                        /*
                        if (input[1].equals("CMD_QUIT")) {
                            this.isRegistered = false;
                            this.isRemoved = true;
                            this.server.addMessage(Color.ANSI_GREEN + "[b session] " + Color.ANSI_RESET + "disconnecting broker " + input[0]);
                            this.disconnect();
                        }
                        else if (input[1].equals("markets")) {
                            this.server.addMesageToSendOnWriteOperation(this.selkey, this.server.getClientsFromTheOtherserver());
                            this.server.setOperationWrite(this.selkey);
                        }
                        else {
                            this.server.setOperationWrite(this.selkey);
                            this.server.addMessage(Color.ANSI_GREEN + "[b session] " + Color.ANSI_RESET + "sent by broker: " + s);
                        }
                        */
                        //System.out.print(Color.ANSI_GREEN + "\n[m broker] " + Color.ANSI_RESET + "sent by broker: " + s);
                    }
                }
            } catch (Throwable t) {
                this.disconnect();
                t.printStackTrace();
            }
        }
    }

    void write() {
        if (this.server.getOperationWrite(this.selkey)) {
            try {
                String responseStr = this.server.getMesageToSendOnWriteOperation(this.selkey);

                ByteBuffer response = ByteBuffer.wrap(responseStr.getBytes());
                this.chan.write(response);

                this.server.setOperationRead(this.selkey);
                this.server.addMessage(Color.ANSI_GREEN + "[b session] " + Color.ANSI_RESET + "done sending data to broker");
            } catch (Throwable t) {
                disconnect();
                t.printStackTrace();
            }
        }
    }
}
