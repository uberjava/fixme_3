package router;

import java.net.InetSocketAddress;
import java.nio.ByteBuffer;
import java.nio.channels.SelectionKey;
import java.nio.channels.SocketChannel;

abstract public class ClientSession {
    boolean isRegistered = false;
    boolean isRemoved = false;
    String id;
    Server server;
    SelectionKey selkey;
    SocketChannel chan;
    ByteBuffer buf = ByteBuffer.allocateDirect(5120);

    void setServer(Server server) {
        this.server = server;
    }

    void setReadKey(SelectionKey key) {
        this.selkey = key;
    }

    void setAcceptedChannel(SocketChannel acceptedChannel) {
        //this.acceptedChannel = acceptedChannel;
        this.chan = acceptedChannel;
    }

    void disconnect() {
        this.server.removeClient(selkey);
        try {
            if (selkey != null)
                selkey.cancel();

            if (chan == null)
                return;

            System.out.println("bye bye " + (InetSocketAddress) chan.getRemoteAddress());
            chan.close();
        } catch (Throwable t) { /** quietly ignore  */ }
    }

    abstract void read();
    abstract void write();

    // GETTER && SETTER

    boolean getIsRegistered() {
        return isRegistered;
    }

    void setIsRegistered(boolean registered) {
        isRegistered = registered;
    }

    String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }
}
