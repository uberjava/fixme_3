package router;

import common.Color;
import common.Settings;

import java.nio.channels.SelectionKey;
import java.util.ArrayList;
import java.util.Scanner;
import java.util.concurrent.Executors;

public class Router {
    private ArrayList<String> serverMessageList = new ArrayList<>();
    private boolean isPrintingMess = false;
    private String inputResultMessage = "";
    private Server marketServer;
    private Server brokerServer;

    public static void main(String[] args) throws Throwable {

        Router router = new Router();

        router.start();
    }

    private void start() {
        this.marketServer = new Server.Builder().
                setRouter(this).
                setType("market").
                setHost(Settings.host).
                setPort(Settings.portMarket).
                setFirstOperation(SelectionKey.OP_READ).
                setClientSession(new MarketSession()).
                build();

        this.brokerServer = new Server.Builder().
                setRouter(this).
                setType("broker").
                setHost(Settings.host).
                setPort(Settings.portBroker).
                setFirstOperation(SelectionKey.OP_READ).
                setClientSession(new BrokerSession()).
                build();

        this.marketServer.setTheOtherServer(this.brokerServer);
        this.brokerServer.setTheOtherServer(this.marketServer);

        try {
            this.marketServer.start();
            this.brokerServer.start();
            this.startRouterManagementConsoleLoop();
        }
        catch (Throwable t) {
            t.printStackTrace();
        }
    }

    private void startRouterManagementConsoleLoop() throws Throwable{
        Executors.newSingleThreadExecutor().submit(() -> {
            try {
                this.routerManagementConsoleLoop();
            }
            catch (Throwable t) {
                t.printStackTrace();
            }
        });
    }

    // Keep only the first 100 messages
    private void printServerMessages() {
        this.isPrintingMess = true;
        while (this.serverMessageList.size() >= 100) {
            this.serverMessageList.remove(0);
        }

        for (String mess : this.serverMessageList) {
            System.out.println(mess);
        }
    }

    // [TODO] do it better thatn this ...
    void addMessage(String mess) {
        while (this.isPrintingMess) {
            // WAIT ...
        }
        this.serverMessageList.add(mess);
    }

    void showConsoleUi() {
        // CLEAR CONSOLE <NOT FOR WINDOWS !>
        System.out.println("----------------------------------------------------------------------");
        System.out.print("\033[H\033[2J");
        System.out.flush();
        this.printServerMessages();
        if (!this.inputResultMessage.equals(""))
            System.out.print(this.inputResultMessage);
        System.out.print(Color.ANSI_PURPLE + "[router]" + Color.ANSI_RESET + " :> ");
        this.isPrintingMess = false;
    }

    private void routerManagementConsoleLoop() {
        String input = "";
        Scanner readInput = new Scanner(System.in);

        while (!input.equals("quit")) {
            this.showConsoleUi();
            input = readInput.nextLine();
            this.inputResultMessage = "";
            switch (input) {
                case "show markets":
                    this.inputResultMessage = this.marketServer.getClients();
                    break;
                case "show brokers":
                    this.inputResultMessage = this.brokerServer.getClients();
                    break;
                default:
                    this.showHelp();
            }
        }
        System.out.println("Bye now !");
        System.exit(0);
    }

    private void showHelp() {
        this.inputResultMessage =
                "Commands:\n" +
                "show markets, show brokers, quit" +
                "\n";
    }
}
