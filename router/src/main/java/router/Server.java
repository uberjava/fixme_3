package router;

import common.Color;
import common.Message;

import java.net.InetSocketAddress;
import java.nio.channels.SelectionKey;
import java.nio.channels.Selector;
import java.nio.channels.ServerSocketChannel;
import java.nio.channels.SocketChannel;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;

public class Server {
    private Router router;
    private String type;
    private String host;
    private int port;
    private int startOperation;
    private ClientSession clientSessionType;
    private Server marketServer;
    private Server brokerServer;

    private Server theOtherServer;

    private ServerSocketChannel serverChannel;
    private Selector selector;
    private SelectionKey serverKey;
    private HashMap<SelectionKey, ClientSession> clientMap = new HashMap<SelectionKey, ClientSession>(); // [LOOK OUT] removed static !
    private HashMap<SelectionKey, Boolean> readyToWrite = new HashMap<SelectionKey, Boolean>();
    private HashMap<SelectionKey, Boolean> readyToRead = new HashMap<SelectionKey, Boolean>();
    private ArrayList<Message> messageFromTheOtherServer = new ArrayList<>();
    private HashMap<SelectionKey, String> messageToSendOnWriteOperation = new HashMap<SelectionKey, String>();

    private boolean startWriting = false;

    private Server(Builder builder) {
        this.router = builder.router;
        this.type = builder.type;
        this.host = builder.host;
        this.port = builder.port;
        this.startOperation = builder.startOperation;
        this.clientSessionType = builder.clientSession;

        try {
            this.setup();
        }
        catch (Throwable t) {
            t.printStackTrace();
        }
    }

    void setTheOtherServer(Server theOtherServer) {
        this.theOtherServer = theOtherServer;
    }

    private void setup() throws Throwable {
        InetSocketAddress listenAddress = new InetSocketAddress(this.host, this.port);
        serverChannel = ServerSocketChannel.open();
        serverChannel.configureBlocking(false);
        serverKey = serverChannel.register(selector = Selector.open(), SelectionKey.OP_ACCEPT);
        serverChannel.bind(listenAddress);
    }

    void start() throws Throwable {
        Executors.newSingleThreadScheduledExecutor().scheduleAtFixedRate(() -> {
            try {
                this.loop();
            }
            catch (Throwable t) {
                t.printStackTrace();
            }
        }, 0, 500, TimeUnit.MILLISECONDS);
    }

    void addMesageToSendOnWriteOperation(SelectionKey key, String mess) {
        this.messageToSendOnWriteOperation.put(key, mess);
    }

    String getMesageToSendOnWriteOperation(SelectionKey key) {
        String result = "no message in HashMap\n";
        if (messageToSendOnWriteOperation.containsKey(key)) {
            result = this.messageToSendOnWriteOperation.get(key);
            this.messageToSendOnWriteOperation.remove(key);
        }
        return result;
    }

    private void loop() throws Throwable {
//        System.out.println("server "+ this.clientSessionType.getClass() +" listening");
        selector.selectNow();
        for (SelectionKey key : selector.selectedKeys()) {
            try {
                if (!key.isValid())
                    continue;

                // ADD A NEW CLIENT TO LIST
                if (key == this.serverKey) {
                    SocketChannel acceptedChannel = this.serverChannel.accept();

                    if (acceptedChannel == null)
                        continue;

                    acceptedChannel.configureBlocking(false);

                    SelectionKey readKey = acceptedChannel.register(selector, this.startOperation);

                    ClientSession newClientSession = clientSessionType.getClass().newInstance();
                    newClientSession.setServer(this);
                    newClientSession.setReadKey(readKey);
                    newClientSession.setAcceptedChannel(acceptedChannel);

                    clientMap.put(readKey, newClientSession);
                    readyToWrite.put(readKey, this.startOperation == SelectionKey.OP_WRITE);
                    readyToRead.put(readKey, this.startOperation == SelectionKey.OP_READ);

                    if (!this.startWriting) {
                        this.startWriting = true;
                        System.out.println();
                    }
                    this.router.addMessage(Color.ANSI_CYAN + "[server] " + Color.ANSI_RESET + "New "+ this.type +" ip: " + acceptedChannel.getRemoteAddress() + ", total "+ this.type +" clients: " + clientMap.size());
                }

                if (key.isWritable() && readyToWrite.get(key)) {
                    ClientSession sesh = clientMap.get(key);

                    if (sesh == null)
                        continue;

                    sesh.write();
                    SocketChannel acceptedChannel = (SocketChannel) key.channel();
                    acceptedChannel.register(selector, SelectionKey.OP_READ);

                    if (!this.startWriting) {
                        this.startWriting = true;
                        System.out.println();
                    }
                }


                if (key.isReadable() && readyToRead.get(key)) {
                    ClientSession sesh = clientMap.get(key);

                    if (sesh == null)
                        continue;

                    sesh.read();

                    if (!sesh.isRemoved) {
                        // IF THIS IS THE FIRST MESSAGE FROM CLIENT, REGISTER IT
                        // THEN SET READ / WRITE MODE
                        if (!sesh.getIsRegistered()) {
                            sesh.setIsRegistered(true);
                            if (this.type.equals("market")) {
                                SocketChannel acceptedChannel = (SocketChannel) key.channel();
                                acceptedChannel.register(this.selector, SelectionKey.OP_WRITE);

                                if (!this.startWriting) {
                                    this.startWriting = true;
                                    System.out.println();
                                }
                                this.router.addMessage(Color.ANSI_CYAN + "[server] " + Color.ANSI_RESET + "registered market with ID: " + sesh.getId());
                                //System.out.println(Color.ANSI_CYAN + "[server] " + Color.ANSI_RESET + "registered market with ID: " + sesh.getId());
                                //this.server.showConsoleUi();
                            } else {
                                SocketChannel acceptedChannel = (SocketChannel) key.channel();
                                acceptedChannel.register(this.selector, SelectionKey.OP_READ);

                                if (!this.startWriting) {
                                    this.startWriting = true;
                                    System.out.println();
                                }
                                this.router.addMessage(Color.ANSI_CYAN + "[server] " + Color.ANSI_RESET + "registered broker with ID: " + sesh.getId());
                                //System.out.println(Color.ANSI_CYAN + "[server] " + Color.ANSI_RESET + "registered broker with ID: " + sesh.getId());
                                //this.server.showConsoleUi();
                            }

                        } else {
                            SocketChannel acceptedChannel = (SocketChannel) key.channel();
                            acceptedChannel.register(this.selector, SelectionKey.OP_WRITE);
                        }
                    }
                }

            } catch (Throwable t) {
                t.printStackTrace();
            }
        }

        this.selector.selectedKeys().clear();

        if (this.startWriting) {
            this.router.showConsoleUi();
            this.startWriting = false;
        }
    }

    void setOperationWrite(SelectionKey key) {
        readyToRead.put(key, false);
        readyToWrite.put(key, true);
    }

    boolean getOperationWrite(SelectionKey key) {
        return  readyToWrite.get(key);
    }

    void setOperationRead(SelectionKey key) {
        readyToWrite.put(key, false);
        readyToRead.put(key, true);
    }

    boolean getOperationRead(SelectionKey key) {
        return  readyToRead.get(key);
    }

    void removeClient(SelectionKey key) {
        clientMap.remove(key);
        readyToWrite.remove(key);
        readyToRead.remove(key);
    }

    String getClients() {
        String result = "";

        try {
            for(HashMap.Entry<SelectionKey, ClientSession> client : this.clientMap.entrySet()) {
                SelectionKey key = client.getKey();
                ClientSession value = client.getValue();

                SocketChannel acceptedChannel = (SocketChannel) key.channel();

                result += value.getId() + " : " + acceptedChannel.getRemoteAddress() + " | ";
            }
            result += "\n";
        }
        catch (Throwable t) {
            t.printStackTrace();
        }

        return result;
    }

    String getClientsFromTheOtherserver() {
        return this.theOtherServer.getClients();
    }

    void addMessage(String mess) {
        this.router.addMessage(mess);
    }

    // --- BUILDER ---
    static class Builder {
        private Router router;
        private String type;
        private String host;
        private int port;
        private int startOperation;
        private ClientSession clientSession;

        Builder setRouter(Router router) {
            this.router = router;
            return this;
        }

        Builder setType(String type) {
            this.type = type;
            return this;
        }

        Builder setHost(String host) {
            this.host = host;
            return this;
        }

        Builder setPort(int port) {
            this.port = port;
            return this;
        }

        Builder setFirstOperation(int startOperation) {
            this.startOperation = startOperation;
            return this;
        }

        Builder setClientSession(ClientSession clientSession) {
            this.clientSession = clientSession;
            return this;
        }

        Server build() {
            return new Server(this);
        }
    }

}
